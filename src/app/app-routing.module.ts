import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { RecupFormComponent } from './recup-list/recup-form/recup-form.component';
import { RecupListComponent } from './recup-list/recup-list.component';
import { SingleRecupComponent } from './recup-list/single-recup/single-recup.component';
import { RecupUpdateComponent } from './recup-list/recup-update/recup-update.component';
import { CreateAssoComponent } from './asso-list/asso-form/create-asso/create-asso.component';
import { UpdateAssoComponent } from './asso-list/asso-form/update-asso/update-asso.component';
import { SingleProduitComponent } from './produit-list/single-produit/single-produit.component';
import { CreateProduitComponent } from './produit-list/produit-form/create-produit/create-produit.component';
import { PersistProduitComponent } from './produit-list/produit-form/persist-produit/persist-produit.component';

const routes: Routes = [
  { path: '', component: LandingPageComponent },
  { path: 'recups', component: RecupListComponent },
  { path: 'recup/new', component: RecupFormComponent },
  { path: 'recup/update/:id', component: RecupUpdateComponent },
  { path: 'recup/:id', component: SingleRecupComponent },
  { path: 'recup/:id/add/product', component: CreateAssoComponent },
  { path: 'recup/:id/update/product/:idProduit', component: UpdateAssoComponent },
  { path: 'produit/addNew/recup/:id', component: CreateProduitComponent },
  { path: 'produit/addNew/validate/:idRecup/:nom/:prix/:unite', component: PersistProduitComponent },
  { path: 'produit/:id', component: SingleProduitComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
