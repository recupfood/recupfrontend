import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { RecupListComponent } from './recup-list/recup-list.component';
import { SingleRecupComponent } from './recup-list/single-recup/single-recup.component';
import { RecupFormComponent } from './recup-list/recup-form/recup-form.component';
import { HeaderComponent } from './header/header.component';
import { AppRoutingModule } from './app-routing.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { registerLocaleData } from '@angular/common';
import * as fr from '@angular/common/locales/fr';
import { RecupUpdateComponent } from './recup-list/recup-update/recup-update.component';
import { ProduitListComponent } from './produit-list/produit-list.component';
import { SingleProduitComponent } from './produit-list/single-produit/single-produit.component';
import { CreateProduitComponent } from './produit-list/produit-form/create-produit/create-produit.component';
import { PersistProduitComponent } from './produit-list/produit-form/persist-produit/persist-produit.component';
import { AssoListComponent } from './asso-list/asso-list.component';
import { SingleAssoComponent } from './asso-list/single-asso/single-asso.component';
import { CreateAssoComponent } from './asso-list/asso-form/create-asso/create-asso.component';
import { UpdateAssoComponent } from './asso-list/asso-form/update-asso/update-asso.component';
import { ServerErrorsInterceptor } from './interceptor/ServerErrorInterceptor';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ToastrModule } from "ngx-toastr";

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    RecupListComponent,
    SingleRecupComponent,
    RecupFormComponent,
    HeaderComponent,
    RecupUpdateComponent,
    ProduitListComponent,
    SingleProduitComponent,
    CreateProduitComponent,
    PersistProduitComponent,
    AssoListComponent,
    SingleAssoComponent,
    CreateAssoComponent,
    UpdateAssoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: "toast-top-center"
    })
  ],
  providers: [
    { provide: LOCALE_ID, 
      useValue: 'fr-FR'
    },
    { provide: HTTP_INTERCEPTORS,
      useClass: ServerErrorsInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor() {
    registerLocaleData(fr.default);
  }
}
