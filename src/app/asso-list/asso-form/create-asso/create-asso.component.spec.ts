import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAssoComponent } from './create-asso.component';

describe('CreateAssoComponent', () => {
  let component: CreateAssoComponent;
  let fixture: ComponentFixture<CreateAssoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateAssoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAssoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
