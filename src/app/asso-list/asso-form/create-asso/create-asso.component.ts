import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Asso } from 'src/app/models/asso.model';
import { Produit } from 'src/app/models/produit.model';
import { AssoService } from 'src/app/services/asso.service';
import { ProduitsService } from '../../../services/produits.service';


@Component({
  selector: 'app-create-asso',
  templateUrl: './create-asso.component.html',
  styleUrls: ['./create-asso.component.scss']
})
export class CreateAssoComponent implements OnInit {

  produitForm!: FormGroup;
  produits!: Produit[];
  produitsSubscraption!: Subscription;
  idRecup = this.route.snapshot.params['id'];

  constructor (private formBuilder: FormBuilder,
               private produitService: ProduitsService,
               private assoService: AssoService,
               private route: ActivatedRoute,
               private router: Router) {}

  ngOnInit()
  {
    this.produitsSubscraption = this.produitService.produitsSubject.subscribe(
      (produits: Produit[]) => {
        this.produits = produits;
      }
    );

    this.produitService.getAll();
    this.initForm();
  }

  initForm() 
  {
    this.produitForm = this.formBuilder.group( {
      idProduit: ['', Validators.required],
      quantite: ['', Validators.required]
    });
  }

  onSubmitForm()
  {
    
    const formValue = this.produitForm.value;

    const newAsso = new Asso(
      this.idRecup,
      formValue['idProduit'],
      formValue['quantite'], 
      0
    );

    this.assoService.create(newAsso)
    .subscribe({
      next: (response) => {
        console.log(response);
        this.router.navigateByUrl(`recup/${this.idRecup}`);
      },
      error: () => {
        this.router.navigate([`recup/${this.idRecup}`]);
      }
    });
  }

  onScrap()
  {
    this.router.navigate([`produit/addNew/recup/${this.idRecup}`]);
  }

}
