import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAssoComponent } from './update-asso.component';

describe('UpdateAssoComponent', () => {
  let component: UpdateAssoComponent;
  let fixture: ComponentFixture<UpdateAssoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateAssoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAssoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
