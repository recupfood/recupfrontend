import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AssoService } from 'src/app/services/asso.service';
import { Asso } from '../../../models/asso.model';
import { Produit } from '../../../models/produit.model';
import { ProduitsService } from '../../../services/produits.service';

@Component({
  selector: 'app-update-asso',
  templateUrl: './update-asso.component.html',
  styleUrls: ['./update-asso.component.scss']
})
export class UpdateAssoComponent implements OnInit {

  produitForm!: FormGroup;
  produit!: Produit;
  assoSubscription!: Subscription;

  constructor(private formBuilder: FormBuilder,
              private assoService: AssoService,
              private produitService: ProduitsService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() 
  {
    const idProduit = +this.route.snapshot.params['idProduit'];
    const idRecup = +this.route.snapshot.params['id'];

    this.assoSubscription = this.assoService.oneProduitSubject.subscribe(
      (produit : Produit) => {
        this.produit = produit;
        console.debug(this.produit);
      }
    );
    
    this.assoService.getByProduitRecup(idRecup, idProduit);
    this.initForm();
  }

  initForm() 
  {
    this.produitForm = this.formBuilder.group( {
      quantite: ['', Validators.required]
    });
  }

  onSubmitForm() 
  {
    const idRecup = +this.route.snapshot.params['id'];
    const idProduit = +this.route.snapshot.params['idProduit'];
    const formValue = this.produitForm.value;
    
    console.debug(formValue);
    const asso = new Asso(
      idRecup,
      idProduit,
      formValue['quantite'],
      0
    );

    this.assoService.update(asso)
    .subscribe({
      next: (response) => {
        console.log('update quantité produit sucess', [response]);
        this.router.navigateByUrl(`recup/${idRecup}`);
      },
      error: () => {
        this.router.navigateByUrl(`recup/${idRecup}`);
      }
    });
  }

}
