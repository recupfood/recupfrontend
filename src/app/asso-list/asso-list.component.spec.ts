import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssoListComponent } from './asso-list.component';

describe('AssoListComponent', () => {
  let component: AssoListComponent;
  let fixture: ComponentFixture<AssoListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssoListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
