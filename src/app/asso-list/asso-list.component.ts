import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Asso } from '../models/asso.model';
import { Produit } from '../models/produit.model';
import { AssoService } from '../services/asso.service';

@Component({
  selector: 'app-asso-list',
  templateUrl: './asso-list.component.html',
  styleUrls: ['./asso-list.component.scss']
})
export class AssoListComponent implements OnInit, OnDestroy {

  produits!: Produit[];
  produitsSubscraption!: Subscription;
  asso!: Asso;

  constructor(private assoService: AssoService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() 
  {
    const id = +this.route.snapshot.params['id'];
    this.produitsSubscraption = this.assoService.produitsSubject.subscribe(
      (produits: Produit[]) => {
        this.produits = produits;
      }
    );
    this.assoService.getAllbyRecup(id);
  }

  ngOnDestroy() 
  {
    this.produitsSubscraption.unsubscribe();
  }

  onViewProduct(id: number) 
  {
    this.router.navigateByUrl(`produit/${id}`);
  }

  CreateProductInRecup() 
  {
    const id = +this.route.snapshot.params['id'];
    this.router.navigate([`recup/${id}/add/product`]);
  }

  updateProductInRecup(idProduit: number)
  {
    const idRecup = +this.route.snapshot.params['id'];
    this.router.navigate([`recup/${idRecup}/update/product/${idProduit}`]);
  }

  deleteProduitInRecup(idProduit: number) 
  {
    const idRecup = +this.route.snapshot.params['id'];
    this.asso = new Asso(idRecup,idProduit,0,0);
    console.log(this.asso);
    this.assoService.delete(this.asso)
    .subscribe({
      next: () => {
        window.location.reload();
      },
      error: (error) => {
        console.log("asso-list deleteProduitInRecup echec" + error);
      }
    });

  }
}
