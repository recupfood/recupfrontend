import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleAssoComponent } from './single-asso.component';

describe('SingleAssoComponent', () => {
  let component: SingleAssoComponent;
  let fixture: ComponentFixture<SingleAssoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SingleAssoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleAssoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
