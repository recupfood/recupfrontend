import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpEvent,
  HttpErrorResponse,
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { HandleErrorService } from "../services/handle-error.service";
import {catchError, map} from "rxjs/operators";

@Injectable()
export class ServerErrorsInterceptor implements HttpInterceptor 
{
    constructor(private error: HandleErrorService) {}

    public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> 
    {
      return next.handle(request)
      .pipe(
        map((response: HttpEvent<any>) => {
          return response;
        }),
        catchError((error: HttpErrorResponse) => {
          this.error.handleError(error);
          return throwError(() => error);
        })
      );
    }
}

