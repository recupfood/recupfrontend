export class Asso {
    constructor (public idRecup: number,
                 public idProduit: number,
                 public quantite: number | undefined,
                 public prix: number | undefined) {}
}