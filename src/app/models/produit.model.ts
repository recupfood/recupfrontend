export class Produit {

    constructor (public id: number,
                 public nom: string,
                 public quantite?: number,
                 public prix?: number,
                 public unite?: string,
                 public prixUnite?: number,
                 public image?: string) {}
}