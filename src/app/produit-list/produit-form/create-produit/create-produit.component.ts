import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Produit } from 'src/app/models/produit.model';
import { ScraperService } from '../../../services/scraper.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-create-produit',
  templateUrl: './create-produit.component.html',
  styleUrls: ['./create-produit.component.scss']
})
export class CreateProduitComponent implements OnInit, OnDestroy {

  produitCreateForm!: FormGroup;
  produits!: Produit[];
  produitsSubscraption!: Subscription;
  idRecup = this.route.snapshot.params['id'];
  isError = false;

  constructor(private formBuilder: FormBuilder,
              private scraperService: ScraperService,
              private router: Router, 
              private route: ActivatedRoute) { }

  ngOnInit()
  {
    this.initForm();
  }

  ngOnDestroy() 
  {
    this.produitsSubscraption.unsubscribe();
  }

  initForm()
  {
    this.produitCreateForm = this.formBuilder.group( {
      searchProduct: ['', Validators.required]
    });
  }

  onSubmit()
  {
    const formValue = this.produitCreateForm.value;

    let search = formValue['searchProduct'];
    this.streamUserSearch(search);
  }  

  streamUserSearch(search: string) 
  {
    this.produitsSubscraption = this.scraperService.produitsSubject.subscribe(
      (produits: Produit[]) => {
        if(produits.length > 0) {
          this.isError = false;
        }
        if(produits.length == 0) {
          this.isError = true;
        }
        this.produits = produits;
      }
    );
    this.scraperService.getAllNonPersistProduct(search);
  }

  persistProduct(produit: Produit) 
  {
    this.router.navigate([`produit/addNew/validate/${this.idRecup}/${produit.nom}/${produit.prix}/${produit.unite}`]);
  }

  retourErreur() 
  {
    window.location.reload();
  }
}
