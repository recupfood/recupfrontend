import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersistProduitComponent } from './persist-produit.component';

describe('UpdateProduitComponent', () => {
  let component: PersistProduitComponent;
  let fixture: ComponentFixture<PersistProduitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersistProduitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersistProduitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
