import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { Asso } from 'src/app/models/asso.model';
import { ProduitsService } from 'src/app/services/produits.service';
import { Produit } from '../../../models/produit.model';
import { AssoService } from '../../../services/asso.service';

@Component({
  selector: 'app-persist-produit',
  templateUrl: './persist-produit.component.html',
  styleUrls: ['./persist-produit.component.scss']
})
export class PersistProduitComponent implements OnInit {

  produit = new Produit(0,'');
  produitSubscraption!: Subscription;
  idRecup = this.route.snapshot.params['idRecup'];

  constructor(private produitService: ProduitsService,
              private assoService: AssoService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit()
  {
    this.produit.nom = this.route.snapshot.params['nom'];
    this.produit.prix = this.route.snapshot.params['prix'];
    this.produit.unite = this.route.snapshot.params['unite'];
  }

  onSubmit(form: NgForm) 
  {
    this.produit.quantite = form.value.quantite;
    this.validateProduct(this.produit);
  }


  validateProduct(produit: Produit)
  {
    this.produitService.create(produit)
      .pipe(
        map((response: any) => { return this.setPrixProduit(response) }), 
        map(() => { return this.addToRecup(this.produit)})
      ).subscribe({ 
        next: () => { this.router.navigateByUrl(`recup/${this.idRecup}`) },
        error: () => {
          this.router.navigate([`recup/${this.idRecup}`]);
        }
      });
  }

  setPrixProduit(response: any) 
  {
    this.produit.id = response.id;
    return this.produitService.persistPrixProduit(this.produit).subscribe();
  }

  addToRecup(produit: any)
  {
    const newAsso = new Asso(
      this.idRecup,
      produit.id,
      this.produit.quantite,
      this.produit.prix
    );

    this.assoService.create(newAsso)
    .subscribe({
      next: () => {
        this.router.navigateByUrl(`recup/${this.idRecup}`);
      },
      error: () => {
        this.router.navigateByUrl(`recup/${this.idRecup}`);
      }
    });
  }

}
