import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Produit } from '../models/produit.model';
import { ProduitsService } from '../services/produits.service';

@Component({
  selector: 'app-produit-list',
  templateUrl: './produit-list.component.html',
  styleUrls: ['./produit-list.component.scss']
})
export class ProduitListComponent implements OnInit {

  produits!: Produit[];
  produitsSubscraption!: Subscription;

  constructor(private produitService: ProduitsService) { }

  ngOnInit() 
  {
    this.produitsSubscraption = this.produitService.produitsSubject.subscribe(
      (produits: Produit[]) => {
        this.produits = produits;
      }
    );

    this.produitService.getAll();
  }
}
