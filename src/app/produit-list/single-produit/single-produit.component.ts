import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Produit } from '../../models/produit.model';
import { ProduitsService } from '../../services/produits.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-single-produit',
  templateUrl: './single-produit.component.html',
  styleUrls: ['./single-produit.component.scss']
})
export class SingleProduitComponent implements OnInit, OnDestroy {

  produit= new Produit(0,'',0,0);
  produitSubscription!: Subscription;

  constructor(private produitService: ProduitsService,
              private route: ActivatedRoute,
              private location: Location) { }

  ngOnInit(): void {

    const id = +this.route.snapshot.params['id'];

    this.produitSubscription = this.produitService.oneProduitSubject.subscribe(
      (produit: Produit) => {
        this.produit = produit;
      }
    );

    this.produitService.getById(id).then(
      (produit: any) => {
        this.produit = produit;
      }
    );
  }

  ngOnDestroy() {
    this.produitSubscription.unsubscribe();
  }

  onRetour() {
    this.location.back();
  }

}
