import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { RecupsService } from '../../services/recups.service';
import { Recup } from '../../models/recup.model';

@Component({
  selector: 'app-recup-form',
  templateUrl: './recup-form.component.html',
  styleUrls: ['./recup-form.component.scss']
})
export class RecupFormComponent implements OnInit {

  recupForm!: FormGroup;
  date = new Date;

  constructor(private formBuilder: FormBuilder,
              private recupService: RecupsService,
              private router: Router) { }

  ngOnInit() 
  {
    this.initForm();
  }

  initForm() 
  {
    this.recupForm = this.formBuilder.group( {
      nom: ['']
    });
  }

  onSubmitForm() 
  {
    const formValue = this.recupForm.value;

    const newRecup = new Recup(
      0,
      formValue['nom'],
      0,
      new Date,
      []
    );

    this.recupService.create(newRecup)
    .subscribe({
      next: () => {
        this.router.navigate(['/recups']);
      },
      error: () => {
        this.router.navigate(['']);
      }
    });
  }

}
