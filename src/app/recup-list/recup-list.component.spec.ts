import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecupListComponent } from './recup-list.component';

describe('RecupListComponent', () => {
  let component: RecupListComponent;
  let fixture: ComponentFixture<RecupListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecupListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecupListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
