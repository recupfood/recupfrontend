import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Recup } from '../models/recup.model';
import { RecupsService } from '../services/recups.service';

@Component({
  selector: 'app-recup-list',
  templateUrl: './recup-list.component.html',
  styleUrls: ['./recup-list.component.scss']
})
export class RecupListComponent implements OnInit, OnDestroy {

  recups!: Recup[];
  recupsSubscription!: Subscription;

  constructor(private recupsService: RecupsService,
              private router: Router) { }

  ngOnInit() 
  {
    this.recupsSubscription = this.recupsService.recupsSubject.subscribe(
      (recups: Recup[]) => {
        this.recups =  recups;
      }
    );
    this.recupsService.getAll();
  }

  ngOnDestroy() 
  {
    this.recupsSubscription.unsubscribe();
  }

  onViewRecup(id: number) 
  {
    this.router.navigateByUrl(`recup/${id}`);
  }

}
