import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecupUpdateComponent } from './recup-update.component';

describe('RecupUpdateComponent', () => {
  let component: RecupUpdateComponent;
  let fixture: ComponentFixture<RecupUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecupUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecupUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
