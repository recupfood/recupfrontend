import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Recup } from 'src/app/models/recup.model';
import { RecupsService } from 'src/app/services/recups.service';

@Component({
  selector: 'app-recup-update',
  templateUrl: './recup-update.component.html',
  styleUrls: ['./recup-update.component.scss']
})
export class RecupUpdateComponent implements OnInit {

  recupForm!: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private recupService: RecupsService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() 
  {
    this.initForm();
  }

  initForm() 
  {
    this.recupForm = this.formBuilder.group( {
      nom: ['']
    });
  }

  onSubmitForm() 
  {
    const id = +this.route.snapshot.params['id'];
    const formValue = this.recupForm.value;
    
    const recup = new Recup(
      id,
      formValue['nom'],
      0,
      new Date,
      []
    );

    this.recupService.update(recup)
    .subscribe({
      next: () => {
        this.router.navigate(['/recups']);
      },
      error: () => {
        this.router.navigate([`/recup/${id}`]);
      }
    });
  }

}
