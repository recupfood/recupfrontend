import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleRecupComponent } from './single-recup.component';

describe('SingleRecupComponent', () => {
  let component: SingleRecupComponent;
  let fixture: ComponentFixture<SingleRecupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SingleRecupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleRecupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
