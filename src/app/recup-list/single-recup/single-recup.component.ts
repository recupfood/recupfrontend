import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Recup } from 'src/app/models/recup.model';
import { RecupsService } from '../../services/recups.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-single-recup',
  templateUrl: './single-recup.component.html',
  styleUrls: ['./single-recup.component.scss']
})
export class SingleRecupComponent implements OnInit, OnDestroy {

  recup = new Recup(0,'',0,new Date,[]);
  recupSubscription!: Subscription;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private recupsService: RecupsService) { }
  

  ngOnInit(): void 
  {
    const id = +this.route.snapshot.params['id'];
    
    this.recupSubscription = this.recupsService.oneRecupSubject.subscribe(
      (recup: Recup) => {
        this.recup = recup;
      }
    );

    this.recupsService.getById(id).then(
      (recup: any) => {
        this.recup = recup;
      }
    );
  }

  ngOnDestroy() 
  {
    this.recupSubscription.unsubscribe();
  }

  onBack() 
  {
    this.router.navigateByUrl(`/recups`);
  }

  updateRecup()
  {
    const id = +this.route.snapshot.params['id'];
    this.router.navigate(['/recup/update/', id]);
  }

  deleteRecup() 
  {
    const id = +this.route.snapshot.params['id'];
    
    this.recupsService.delete(id)
    .subscribe({
      next: () => {
        this.router.navigateByUrl(`/recups`);
      },
      error: () => {
        this.router.navigate([`/recup/${id}`]);
      }
    });
  }

}
