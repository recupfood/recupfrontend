import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import { Produit } from '../models/produit.model';
import { Asso } from '../models/asso.model';

@Injectable({
  providedIn: 'root'
})
export class AssoService {

  apiUrl = environment.apiUrl;

  produits!: Produit[];
  produit!: Produit;
  asso!: Asso;
  produitsSubject = new Subject<Produit[]>();
  oneProduitSubject = new Subject<Produit>();

  constructor(private http: HttpClient) { }

  emitProduits() 
  {
    this.produitsSubject.next(this.produits);
  }

  emitOneProduit()
  {
    this.oneProduitSubject.next(this.produit);
  }

  getAllbyRecup(id: number) 
  {
    this.produits = [];
    this.http.get<any[]>(`${this.apiUrl}produit/recup/${id}`)
    .subscribe({
      next: (response) => {
        response.forEach((produit) => {
          this.produits.push(new Produit(produit.produit.id, produit.produit.nom, produit.quantite, produit.dernierPrix))  
        });
        this.emitProduits();
      },
      error: (error) => {
        console.error("AssoService: getAllbyRecup echec" + error);  
      }
    });
  }

  getByProduitRecup(idRecup: number, idProduit: number)
  {
    this.produits = [];
    this.http.get<any[]>(`${this.apiUrl}produit/recup/${idRecup}`)
    .subscribe({
      next: (response) => {
        response.forEach((produit) => {
          if (produit.produit.id === idProduit) {
            this.produit = new Produit(produit.produit.id, produit.produit.nom, produit.quantite, produit.dernierPrix);
            this.emitOneProduit();
          }
        })
      },
      error: (error) => {
        console.error("AssoService: getAllbyRecup echec" + error);  
      }
    })
  }

  create(asso: Asso): Observable<any>
  {
    const produitNom = JSON.stringify({ "produit": asso.idProduit, "quantite": asso.quantite });
    return this.http.post(`${environment.apiUrl}recup/${asso.idRecup}/produit`, produitNom);
  }

  update(asso: Asso): Observable<any> 
  {
    const produitNom = JSON.stringify({ "produit": asso.idProduit, "quantite": asso.quantite });
    return this.http.put(`${environment.apiUrl}recup/${asso.idRecup}/produit`, produitNom);
  }

  delete(asso: Asso): Observable<any>
  {
    const options = {
      Headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        produit: asso.idProduit
      }
    };
    return this.http.delete(`${environment.apiUrl}recup/${asso.idRecup}/produit`, options);
  }
}  
