import { Injectable } from '@angular/core';
import { HttpErrorResponse } from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';
import {Location} from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class HandleErrorService {

  constructor(private taostrs: ToastrService, private location: Location) {}

  public handleError(error: HttpErrorResponse) {
    let errorMessage: string = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = `une erreur s'est produite: ${error.error.message}`;
    } else {
      switch (error.status) {
        case 400:
          errorMessage = "Mauvaise requête.";
          break;
        case 401:
          errorMessage = "Vous devez être connecté pour faire ceci.";
          break;
        case 403:
          errorMessage = "Vous n'avez pas les permissions pour accèder à ces ressources.";
          break;
        case 404:
          errorMessage = "La ressource demandé n'existe pas.";
          break; 
        case 409:
          errorMessage = "La ressource existe déjà.";
          break;
        case 412:
          errorMessage = "Condition préalable éronné.";
          break;
        case 0:
        case 500:
          errorMessage = "Problème serveur.";
          break;
        case 503:
          errorMessage = "Le service demandé n'est pas disponible.";
          break;
        case 422:
          errorMessage = "Erreur de validation!";
          break;
        default:
          errorMessage = "Oups un erreur est survenu!";
      }
    }
    this.taostrs.error(errorMessage);
  }
}
