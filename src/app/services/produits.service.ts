import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import { Produit } from '../models/produit.model';

@Injectable({
  providedIn: 'root'
})
export class ProduitsService {

  apiUrl = environment.apiUrl + "produit";

  produits!: Produit[];
  produit!: Produit;
  produitsSubject = new Subject<Produit[]>();
  oneProduitSubject = new Subject<Produit>();

  constructor(private http: HttpClient) { }

  emitProduits() 
  {
    this.produitsSubject.next(this.produits);
  }

  emitOneProduit()
  {
    this.oneProduitSubject.next(this.produit);
  }

  getAll()
  {
    this.produits = [];
    this.http.get<any[]>(this.apiUrl)
    .subscribe({
      next: (response) => {
        response.forEach((produit) => {
          this.produits.push(new Produit(produit.id, produit.nom, 0, 0));
        });
        this.emitProduits();
      },
      error: (error) => {
        console.error("ProduitsService: getAll echec" + error);  
      }
    });
  }

  getById(idProduct: number) 
  {
    return new Promise((resolve, reject) => {
      this.http.get<any>(`${this.apiUrl}/${idProduct}`)
        .subscribe({
          next: (response) => {
            this.produit = new Produit(response.id, response.nom, response.quantite, response.prix);
            resolve(this.produit);
            this.emitOneProduit();
          },
          error: (error) => {
            reject("ProduitsService: getById echec" + error);
          }
        });
    });
  }

  create(produit: Produit)
  {
    if(produit.unite == "pce") {
      produit.unite = "u";
    }
    if (produit.unite == "kg") {
      produit.unite = "k";
    }
    
    const newProduit = JSON.stringify({ "nom": produit.nom, "unite": produit.unite });
    return this.http.post(this.apiUrl, newProduit);
    
  }

  persistPrixProduit(produit: Produit)
  {
    console.log(produit);
    const newPrix = JSON.stringify({ "prix": produit.prix, "produit": produit.id })
    return this.http.post(`${environment.apiUrl}prix`, newPrix);
  }

}
