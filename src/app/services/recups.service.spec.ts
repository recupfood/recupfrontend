import { TestBed } from '@angular/core/testing';

import { RecupsService } from './recups.service';

describe('RecupsService', () => {
  let service: RecupsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RecupsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
