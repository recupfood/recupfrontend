import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Recup } from '../models/recup.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class RecupsService {

  apiUrl = environment.apiUrl + 'recup';
  
  recups!: Recup[];
  recup!: Recup;
  recupsSubject = new Subject<Recup[]>();
  oneRecupSubject = new Subject<Recup>();

  constructor(private http: HttpClient) { }

  emitRecups() 
  {
    this.recupsSubject.next(this.recups);
  }

  emitOneRecup() 
  {
    this.oneRecupSubject.next(this.recup);
  }

  getAll() 
  {
    this.recups = [];
    this.http.get<any[]>(this.apiUrl)
    .subscribe({
      next: (response) => {
        response.forEach((recup) => {
          this.recups.push(new Recup(recup.id, recup.nom, recup.somme, new Date(recup.date), recup.produits))
        });
        this.emitRecups();
      },
      error: (error) => {
        console.error("RecupsService: getAll echec" + error);
      }
    });
  }

  getById(id: number) 
  {
    return new Promise((resolve, reject) => {
      this.http.get<any>(`${this.apiUrl}/${id}`)
      .subscribe({
        next: (response) => {
          this.recup = new Recup(response.id, response.nom, response.somme, new Date(response.date), response.produits);
          resolve(this.recup);
          this.emitOneRecup();
        }, 
        error: (error) => {
          reject("RecupsService: getById echec" + error);
        }
      });
    });
  }

  create(recup: Recup): Observable<any>
  {
    const recupNom = JSON.stringify({ "nom": recup.nom });
    return this.http.post(this.apiUrl, recupNom);
  }

  update(recup: Recup): Observable<any> 
  {
    const id = recup.id;
    const recupNom = JSON.stringify({ "nom": recup.nom });
    return this.http.put(`${this.apiUrl}/${id}`, recupNom);
  }

  delete(id: number): Observable<any>
  {
    return this.http.delete(`${this.apiUrl}/${id}`);
  }

}
