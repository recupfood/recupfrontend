import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Produit } from '../models/produit.model';

@Injectable({
  providedIn: 'root'
})
export class ScraperService {

  apiUrl = environment.apiUrl + 'scrap';

  produits!: Produit[];
  produitsSubject = new Subject<Produit[]>();

  constructor(private http: HttpClient) { }

  emitProduits() 
  {
    this.produitsSubject.next(this.produits);
  }

  getAllNonPersistProduct(search: string) 
  {
    this.produits = [];
    let i = 1;
    this.http.get<any[]>(`${this.apiUrl}?s=${search}`)// change to : `${this.apiUrl}?s=${search}` --> `http://localhost:8080/test/scrap`
    .subscribe({
      next: (response) => {
        console.log("ScraperService: getResultResearch success" + response);
        response.forEach((produit) => {
          this.produits.push(new Produit(i, produit.nom, 0, produit.prix, produit.prixUnite.unite, produit.prixUnite.prixUnite));
          i++;
        });
        this.emitProduits();
      },
      error: (error) => {
        console.error("ScraperService: getresultReasearch echec" + error);  
      }
    });
  }
}
